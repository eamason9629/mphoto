const app = {
    getPath: jest.fn().mockReturnValue('/a/test/path')
}

export {app as app};

const electron = {
    app: app
}

export default electron;