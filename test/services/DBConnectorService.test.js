import fs from 'fs';
import sut from '../../mPhoto/services/DBConnectorService';

jest.mock('fs');

describe('DBConnectorService', () => {
    beforeEach(() => {
        jest.clearAllMocks();
        jest.restoreAllMocks();
    });

    test('constructDBLocation', () => {
        //when
        let result = sut.constructDBLocation('meh');

        //then
        expect(result).toEqual('/Users/A1147203/code/mphoto/db/meh.db');
    });

    test('createNamespaceDB', () => {
        //setup
        let writeNamespace = jest.spyOn(sut, 'writeNamespace').mockImplementation(() => {});

        //when
        sut.createNamespaceDB('meh');

        //then
        expect(writeNamespace).toHaveBeenCalledTimes(1);
        expect(writeNamespace).toHaveBeenCalledWith('meh');
        expect(sut.getAll('meh')).toEqual([]);
    });

    test('loadNamespace', () => {
        //setup
        let constructDBLocation = jest.spyOn(sut, 'constructDBLocation').mockImplementation(() => { return 'lol'; });
        let createNamespaceDB = jest.spyOn(sut, 'createNamespaceDB').mockImplementation(() => {});
        fs.existsSync.mockReturnValueOnce(true).mockReturnValueOnce(false);
        fs.readFileSync.mockImplementation(() => {
            return JSON.stringify({
                data: {
                    one: {
                        two: 'three'
                    }
                },
                namespace: 'lol',
                schemaVersion: 'V1'
            });
        });

        //when - dbLocation exists
        sut.loadNamespace('lol');

        //then
        expect(constructDBLocation).toHaveBeenCalledTimes(1);
        expect(constructDBLocation).toHaveBeenCalledWith('lol');
        expect(fs.existsSync).toHaveBeenCalledTimes(1);
        expect(fs.existsSync).toHaveBeenCalledWith('lol');
        expect(createNamespaceDB).toHaveBeenCalledTimes(0);
        expect(fs.readFileSync).toHaveBeenCalledTimes(1);
        expect(fs.readFileSync).toHaveBeenCalledWith('lol');
        expect(sut.getAll('lol')).toEqual([{two: 'three'}]);

        //when - dbLocation does not exist
        sut.loadNamespace('lol');

        //then
        expect(constructDBLocation).toHaveBeenCalledTimes(2);
        expect(constructDBLocation).toHaveBeenCalledWith('lol');
        expect(fs.existsSync).toHaveBeenCalledTimes(2);
        expect(fs.existsSync).toHaveBeenCalledWith('lol');
        expect(createNamespaceDB).toHaveBeenCalledTimes(1);
        expect(fs.readFileSync).toHaveBeenCalledTimes(2);
        expect(fs.readFileSync).toHaveBeenCalledWith('lol');
        expect(sut.getAll('lol')).toEqual([{two: 'three'}]);
    });

    test('writeNamespace', () => {
        //setup
        let testData = {
            data: {
                one: {
                    two: 'three'
                }
            },
            namespace: 'nice',
            schemaVersion: 'V1'
        };
        let constructDBLocation = jest.spyOn(sut, 'constructDBLocation').mockImplementation(() => { return 'nice'; });
        fs.existsSync.mockReturnValue(true);
        fs.readFileSync.mockImplementation(() => {
            return JSON.stringify(testData);
        });
        sut.loadNamespace('nice');

        //when
        sut.writeNamespace('nice');

        //then
        expect(constructDBLocation).toHaveBeenCalledTimes(2);
        expect(constructDBLocation).toHaveBeenCalledWith('nice');
        expect(fs.writeFile).toHaveBeenCalledTimes(1);
        expect(fs.writeFile).toHaveBeenCalledWith('nice', JSON.stringify(testData));
    });

    test('queryNamespace', () => {
        //setup
        let testData = [{two: 'three'}];
        let queryMethod = jest.fn().mockReturnValueOnce(true).mockReturnValueOnce(false);
        let getAll = jest.spyOn(sut, 'getAll').mockReturnValue(testData);

        //when
        let result = sut.queryNamespace('kekw', queryMethod);

        //then
        expect(result).toEqual([{two: 'three'}]);
        expect(getAll).toHaveBeenCalledTimes(1);
        expect(getAll).toHaveBeenCalledWith('kekw');
        expect(queryMethod).toHaveBeenCalledTimes(1);
        expect(queryMethod).toHaveBeenCalledWith(testData[0], 0, testData);

        //when
        result = sut.queryNamespace('kekw', queryMethod);

        //then
        expect(result).toEqual([]);
        expect(getAll).toHaveBeenCalledTimes(2);
        expect(getAll).toHaveBeenCalledWith('kekw');
        expect(queryMethod).toHaveBeenCalledTimes(2);
        expect(queryMethod).toHaveBeenCalledWith(testData[0], 0, testData);
    });

    test('getAll, get, and put', () => {
        //setup
        let constructDBLocation = jest.spyOn(sut, 'constructDBLocation').mockImplementation(() => { return 'beep'; });
        let writeNamespace = jest.spyOn(sut, 'writeNamespace').mockImplementation(() => {});
        fs.existsSync.mockReturnValue(true);
        fs.readFileSync.mockImplementation(() => {
            return JSON.stringify({
                data: {
                    beep: {
                        imma: 'sheep'
                    }
                },
                namespace: 'beep',
                schemaVersion: 'V1'
            });
        });

        //when - getAll
        let result = sut.getAll('beep');

        //then
        expect(result).toEqual([{imma: 'sheep'}]);
        expect(constructDBLocation).toHaveBeenCalledTimes(1);

        //when - get
        result = sut.get('beep', 'beep');

        //then
        expect(result).toEqual({imma: 'sheep'});
        expect(constructDBLocation).toHaveBeenCalledTimes(1);

        //when - put
        sut.put('beep', 'meow', {meow: 'Imma cow'});

        //then
        expect(sut.get('beep', 'meow')).toEqual({meow: 'Imma cow'});
        expect(sut.getAll('beep')).toEqual([{imma: 'sheep'}, {meow: 'Imma cow'}]);
        expect(constructDBLocation).toHaveBeenCalledTimes(1);
        expect(writeNamespace).toHaveBeenCalledTimes(1);
        expect(writeNamespace).toHaveBeenCalledWith('beep');
    });
});
