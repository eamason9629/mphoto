import sut, {SORT_ORDER} from '../../mPhoto/services/ImageFileService';

describe('ImageFileService', () => {
    test.each([
        [SORT_ORDER.dateAsc, '/Users/A1147203/code/mphoto/test/data/images/themoon.jpg', '/Users/A1147203/code/mphoto/test/data/images/fortnite1.jpeg', true],
        [SORT_ORDER.dateAsc, '/Users/A1147203/code/mphoto/test/data/images/fortnite1.jpeg', '/Users/A1147203/code/mphoto/test/data/images/themoon.jpg', false],
        [SORT_ORDER.dateDesc, '/Users/A1147203/code/mphoto/test/data/images/themoon.jpg', '/Users/A1147203/code/mphoto/test/data/images/fortnite1.jpeg', false],
        [SORT_ORDER.dateDesc, '/Users/A1147203/code/mphoto/test/data/images/fortnite1.jpeg', '/Users/A1147203/code/mphoto/test/data/images/themoon.jpg', true],
        [SORT_ORDER.modDateAsc, '/Users/A1147203/code/mphoto/test/data/images/themoon.jpg', '/Users/A1147203/code/mphoto/test/data/images/fortnite1.jpeg', true],
        [SORT_ORDER.modDateAsc, '/Users/A1147203/code/mphoto/test/data/images/fortnite1.jpeg', '/Users/A1147203/code/mphoto/test/data/images/themoon.jpg', false],
        [SORT_ORDER.modDateDesc, '/Users/A1147203/code/mphoto/test/data/images/themoon.jpg', '/Users/A1147203/code/mphoto/test/data/images/fortnite1.jpeg', false],
        [SORT_ORDER.modDateDesc, '/Users/A1147203/code/mphoto/test/data/images/fortnite1.jpeg', '/Users/A1147203/code/mphoto/test/data/images/themoon.jpg', true],
        [SORT_ORDER.fileSizeAsc, '/Users/A1147203/code/mphoto/test/data/images/themoon.jpg', '/Users/A1147203/code/mphoto/test/data/images/fortnite1.jpeg', false],
        [SORT_ORDER.fileSizeAsc, '/Users/A1147203/code/mphoto/test/data/images/fortnite1.jpeg', '/Users/A1147203/code/mphoto/test/data/images/themoon.jpg', true],
        [SORT_ORDER.fileSizeDesc, '/Users/A1147203/code/mphoto/test/data/images/themoon.jpg', '/Users/A1147203/code/mphoto/test/data/images/fortnite1.jpeg', true],
        [SORT_ORDER.fileSizeDesc, '/Users/A1147203/code/mphoto/test/data/images/fortnite1.jpeg', '/Users/A1147203/code/mphoto/test/data/images/themoon.jpg', false],
        [SORT_ORDER.fileNameAsc, '/Users/A1147203/code/mphoto/test/data/images/themoon.jpg', '/Users/A1147203/code/mphoto/test/data/images/fortnite1.jpeg', false],
        [SORT_ORDER.fileNameAsc, '/Users/A1147203/code/mphoto/test/data/images/fortnite1.jpeg', '/Users/A1147203/code/mphoto/test/data/images/themoon.jpg', true],
        [SORT_ORDER.fileNameDesc, '/Users/A1147203/code/mphoto/test/data/images/themoon.jpg', '/Users/A1147203/code/mphoto/test/data/images/fortnite1.jpeg', true],
        [SORT_ORDER.fileNameDesc, '/Users/A1147203/code/mphoto/test/data/images/fortnite1.jpeg', '/Users/A1147203/code/mphoto/test/data/images/themoon.jpg', false],
    ])('sortImages %s %s %s %s', (sortOrder, image1, image2, expected) => {
        //when
        let result = sut.sortImages(image1, image2, sortOrder);

        //then
        expect(result).toEqual(expected);
    });

    test.each([
        //given                                                                         expected
        ['/Users/A1147203/code/mphoto/test/data/images/best-buy.png',                   true],
        ['/Users/A1147203/code/mphoto/test/data/images/fortnite1.jpeg',                 true],
        ['/Users/A1147203/code/mphoto/test/data/images/themoon.jpg',                    true],
        ['/Users/A1147203/code/mphoto/test/data/images/embeddedFolder/imposter.jpeg',   true],
        ['/Users/A1147203/code/mphoto/test/data/images/embeddedFolder/insideout.jpg',   true],
        ['/somefile/no.mov',                                                            false],
        ['/somefile/nah.gif',                                                           false],
        ['/somefile/nein.mpeg',                                                         false],
    ])('shouldIncludeFile %s => %s', (given, expected) => {
        //expect
        expect(sut.shouldIncludeFile(given)).toEqual(expected);
    });

    test.each([
        //given                                                                         expected
        ['/Users/A1147203/code/mphoto/test/data/images/best-buy.png',                   'best-buy.png'],
        ['/Users/A1147203/code/mphoto/test/data/images/fortnite1.jpeg',                 'fortnite1.jpeg'],
        ['/Users/A1147203/code/mphoto/test/data/images/themoon.jpg',                    'themoon.jpg'],
        ['/Users/A1147203/code/mphoto/test/data/images/embeddedFolder/imposter.jpeg',   'imposter.jpeg'],
        ['/Users/A1147203/code/mphoto/test/data/images/embeddedFolder/insideout.jpg',   'insideout.jpg'],
        ['/somefile/no.mov',                                                            'no.mov'],
        ['/somefile/nah.gif',                                                           'nah.gif'],
        ['/somefile/nein.mpeg',                                                         'nein.mpeg'],
    ])('shortNameFromFullPath %s => %s', (given, expected) => {
        //expect
        expect(sut.shortNameFromFullPath(given)).toEqual(expected);
    });

    test('buildFileList', () => {
        //setup
        let expected = [
            '/Users/A1147203/code/mphoto/test/data/images/best-buy.png',
            '/Users/A1147203/code/mphoto/test/data/images/fortnite1.jpeg',
            '/Users/A1147203/code/mphoto/test/data/images/themoon.jpg',
            '/Users/A1147203/code/mphoto/test/data/images/embeddedFolder/imposter.jpeg',
            '/Users/A1147203/code/mphoto/test/data/images/embeddedFolder/insideout.jpg',
        ].sort();

        //expect
        expect(sut.buildFileList('/Users/A1147203/code/mphoto/test/data/images').sort()).toEqual(expected);
    });

    test('generateStats', () => {
        //hard coded stats pulled from the file itself
        expect(sut.generateStats('/Users/A1147203/code/mphoto/test/data/images/best-buy.png')).toEqual({
            fullPath: '/Users/A1147203/code/mphoto/test/data/images/best-buy.png',
            shortName: 'best-buy.png',
            creationDate: new Date(1607967696000),
            creationMS: 1607967695999.9998,
            modifiedDate: new Date(1607967696000),
            modifiedMS: 1607967696000,
            fileSize: 57089
        });
    });
});
