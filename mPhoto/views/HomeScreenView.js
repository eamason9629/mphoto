import React, { Component } from 'react';
import {SCREENS} from '../controllers/ApplicationController';

class HomeScreenView extends Component {
    constructor(options) {
        super();

        this.state = {
            changeScreen: options.changeScreen
        };

        this.onChangeScreen = this.onChangeScreen.bind(this);
    }

    onChangeScreen(event) {
        this.state.changeScreen(event.currentTarget.dataset.target);
    }

    render() {
        return (
            <div>
                <h1>How would you like to proceed?</h1>
                <div className="btn-group" role="group" aria-label="Actions">
                    <button type="button" className="btn btn-primary" data-target={SCREENS.browse} onClick={this.onChangeScreen}>Browse Images</button>
                    <button type="button" className="btn btn-primary" data-target={SCREENS.search} onClick={this.onChangeScreen}>Search Images</button>
                    <button type="button" className="btn btn-primary" data-target={SCREENS.memories} onClick={this.onChangeScreen}>View Memories</button>
                    <button type="button" className="btn btn-primary" data-target={SCREENS.configure} onClick={this.onChangeScreen}>Configure mPhoto</button>
                    <button type="button" className="btn btn-primary" data-target={SCREENS.history} onClick={this.onChangeScreen}>Navigation History</button>
                </div>
            </div>
        )
    }
}

export default HomeScreenView;