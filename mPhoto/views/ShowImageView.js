import React, { Component } from 'react';
import {SCREENS} from '../controllers/ApplicationController';
import imageFileService from '../services/ImageFileService';

class ShowImageView extends Component {
    constructor(options) {
        super();

        this.state = {
            imagePath: options.currentImage,
            stats: {
                creationDate: new Date(),
                modifiedDate: new Date()
            }
        };
    }

    componentDidMount() {
        let stats = imageFileService.generateStats(this.state.imagePath);
        this.setState({stats: stats});
    }

    render() {
        return (
            <div>
                <div className='row'>
                    <div className='col-2'><strong>Filename:</strong> {this.state.stats.shortName}</div>
                    <div className='col-2'><strong>Created:</strong> {this.state.stats.creationDate.toString()}</div>
                    <div className='col-2'><strong>Modified:</strong> {this.state.stats.modifiedDate.toString()}</div>
                    <div className='col-2'><strong>File Size:</strong> {this.state.stats.fileSize} bytes</div>
                    <div className='col-4'><strong>Full Path:</strong> {this.state.imagePath}</div>
                </div>
                <div className='row'>
                    <div className='col-12'>
                        <img src={'file://' + this.state.imagePath} alt={this.state.imagePath} />
                    </div>
                </div>
            </div>
        );
    }
}

export default ShowImageView;