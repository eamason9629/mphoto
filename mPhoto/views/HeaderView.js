import React, { Component } from 'react';
import {SCREENS} from '../controllers/ApplicationController';

class HeaderView extends Component {
    constructor(options) {
        super();

        this.state = {
            changeScreen: options.changeScreen
        }

        this.onChangeScreen = this.onChangeScreen.bind(this);
    }

    onChangeScreen(event) {
        this.state.changeScreen(event.currentTarget.dataset.target);
    }

    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="container-fluid">
                    <a className="navbar-brand" href="#" data-target={SCREENS.home} onClick={this.onChangeScreen}>mPhoto</a>
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <a className="nav-link active" aria-current="page" href="#" data-target={SCREENS.home} onClick={this.onChangeScreen}>Home</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#" data-target={SCREENS.browse} onClick={this.onChangeScreen}>Browse</a>
                        </li>
                        <li className="nav-item">
                          <a className="nav-link disabled" href="#" tabIndex="-1" aria-disabled="true">Hi Username!</a>
                        </li>
                    </ul>
                    <form className="d-flex">
                        <input className="form-control me-2" type="search" placeholder="Search" aria-label="Search" />
                        <button className="btn btn-outline-success" type="submit">Search</button>
                    </form>
                </div>
            </nav>
        )
    }
}

export default HeaderView;