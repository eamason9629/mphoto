import React, { Component } from 'react';
import {SCREENS} from '../controllers/ApplicationController';
import imageFileService, {SORT_ORDER} from '../services/ImageFileService';

const COLUMN_CHOICES = {
    one: { prettyName: '1 image', division: 1 },
    two: { prettyName: '2 images', division: 2 },
    three: { prettyName: '3 images', division: 3 },
    four: { prettyName: '4 images', division: 4 },
    six: { prettyName: '6 images', division: 6 },
    twelve: { prettyName: '12 images', division: 12 }
};

class BrowseImagesView extends Component {
    constructor(options) {
        super();

        this.state = {
            loading: true,
            changeScreen: options.changeScreen,
            sortOrder: SORT_ORDER.dateDesc,
            numColumns: COLUMN_CHOICES.twelve,
            images: [],
        };

        this.renderImage = this.renderImage.bind(this);
        this.sortImages = this.sortImages.bind(this);
        this.onChangeSortOrder = this.onChangeSortOrder.bind(this);
        this.onChangeColumns = this.onChangeColumns.bind(this);
        this.onImageClick = this.onImageClick.bind(this);
    }

    componentDidMount() {
        let images = imageFileService.loadImages();
        this.setState({loading: false, images: images});
    }

    sortImages() {
        let result = [];
        let curArray = [];
        this.state.images.sort((image1, image2) => { return imageFileService.sortImages.bind(imageFileService)(image1, image2, this.state.sortOrder)}).forEach(image => {
            curArray.push(image);
            if(curArray.length % this.state.numColumns.division == 0) {
                result.push(curArray);
                curArray = [];
            }
        });
        if(curArray.length > 0) {
            result.push(curArray);
        }
        return result;
    }

    onChangeSortOrder(event) {
        this.setState({sortOrder: event.currentTarget.value});
    }

    onChangeColumns(event) {
        let newChoice = Object.values(COLUMN_CHOICES).find(choice => {
            return choice.division == event.currentTarget.value;
        });
        this.setState({numColumns: newChoice});
    }

    onImageClick(event) {
        this.state.changeScreen(SCREENS.showImage, {currentImage: event.currentTarget.dataset.filename});
    }

    renderImage(index, image, imageIndex) {
        return (
            <div className={'tooltipMain col-' + (12 / this.state.numColumns.division)} key={'row-' + index + '-col-' + imageIndex}>
                <img className='img-thumbnail' src={'file://' + image.fullPath} alt={image.fullPath} data-filename={image.fullPath} onClick={this.onImageClick} />
                <span className='tooltipContent'>
                    <strong>Filename:</strong> {image.shortName}<br />
                    <strong>Created:</strong> {image.creationDate.toString()}<br />
                    <strong>Modified:</strong> {image.modifiedDate.toString()}<br />
                    <strong>File Size:</strong> {image.fileSize} bytes<br />
                </span>
            </div>
        );
    }

    renderImages() {
        let images = this.sortImages();
        return (
            <div className='container-fluid' style={{marginTop: '10px'}} key={this.state.sortOrder + this.state.numColumns.division}>
                <div className='row pb-2'>
                    <select className='form-select' aria-label='Sort Images' value={this.state.sortOrder} onChange={this.onChangeSortOrder}>
                        {Object.values(SORT_ORDER).map(option => {
                            return (<option key={option} value={option}>{option}</option>)
                        })}
                    </select>
                    <select className='form-select' aria-label='Images Per Column' value={this.state.numColumns.division} onChange={this.onChangeColumns}>
                        {Object.values(COLUMN_CHOICES).map(option => {
                            return (<option key={option.prettyName} value={option.division}>{option.prettyName}</option>)
                        })}
                    </select>
                </div>
                {images.map((imageArray, index) => {
                    return (
                        <div className='row pb-2' key={'row-' + index}>
                            {imageArray.map((image, imageIndex) => { return this.renderImage(index, image, imageIndex); })}
                        </div>
                    );
                })}
            </div>
        );
    }

    render() {
        if(this.state.loading) {
            return <div>Loading...</div>
        } else {
            return (this.renderImages());
        }
    }
}

export default BrowseImagesView;