import React, { Component } from 'react';
import NavigationHistoryService from '../services/NavigationHistoryService';

class HomeScreenView extends Component {
    constructor(options) {
        super();

        this.state = {
            history: []
        };
    }

    componentDidMount() {
        this.setState({history: NavigationHistoryService.allHistory()});
    }

    render() {
        return (
            <div>
                <h1>Application Navigation History</h1>
                {this.state.history.map(history => {
                    return <div>{history.currentScreen}: {JSON.stringify(history)}</div>;
                })}
            </div>
        )
    }
}

export default HomeScreenView;