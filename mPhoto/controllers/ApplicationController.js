import React, { Component } from 'react';
import NavigationHistoryService from '../services/NavigationHistoryService';
import HeaderView from '../views/HeaderView';
import BrowseImagesView from '../views/BrowseImagesView';
import HomeScreenView from '../views/HomeScreenView';
import ShowImageView from '../views/ShowImageView';
import NavHistoryView from '../views/NavHistoryView';

const SCREENS = {
    browse: 'browse',
    configure: 'configure',
    history: 'navHistory',
    home: 'home',
    memories: 'memories',
    search: 'search',
    showImage: 'showImage'
};

export {SCREENS as SCREENS};

class ApplicationController extends Component {
    constructor() {
        super();

        this.state = {
            currentScreen: SCREENS.home
        };

        this.changeScreen = this.changeScreen.bind(this);
    }

    componentDidMount() {
        this.setState(NavigationHistoryService.currentScreen());
    }

    changeScreen(newScreen, data) {
        let newData = data ? data : {};
        newData.currentScreen = newScreen;
        NavigationHistoryService.screenChange(newData, this.state);
        this.setState(newData);
    }

    renderScreen() {
        switch(this.state.currentScreen) {
            case SCREENS.browse:
                return <BrowseImagesView changeScreen={this.changeScreen} />;
            case SCREENS.configure:
                return <div>configure</div>;
            case SCREENS.memories:
                return <div>memories</div>;
            case SCREENS.search:
                return <div>search</div>;
            case SCREENS.showImage:
                return <ShowImageView currentImage={this.state.currentImage} />;
            case SCREENS.history:
                return <NavHistoryView />;
            case SCREENS.home:
            default:
                return <HomeScreenView changeScreen={this.changeScreen} />;
        }
    }

    render() {
        return (
            <div className='container-fluid'>
                <HeaderView changeScreen={this.changeScreen} />
                {this.renderScreen()}
            </div>
        )
    }
}

export default ApplicationController;
