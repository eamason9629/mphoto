import fs from 'fs';
import db from './DBConnectorService';

const NAMESPACE = 'images';

const FILE_EXT_PATTERN = /.\.(jpg|jpeg|png)$/i;

const SORT_ORDER = {
    dateAsc: 'Oldest First',
    dateDesc: 'Newest First',
    modDateAsc: 'Oldest Modified First',
    modDateDesc: 'Last Modified First',
    fileSizeAsc: 'File Size Ascending',
    fileSizeDesc: 'File Size Descending',
    fileNameAsc: 'Filename Ascending',
    fileNameDesc: 'Filename Descending'
};

const IMAGE_OBJECT_TEMPLATE = (fullPath = '', shortName = '', creationDate = new Date(0), creationMS = 0, modifiedDate = new Date(0), modifiedMS = 0, fileSize = 0) => {
    return {
        fullPath: fullPath,
        shortName: shortName,
        creationDate: creationDate,
        creationMS: creationMS,
        modifiedDate: modifiedDate,
        modifiedMS: modifiedMS,
        fileSize: fileSize
    };
}

export {SORT_ORDER as SORT_ORDER};

class ImageFileService {
    //public
    sortImages(image1, image2, sortBy = SORT_ORDER.dateDesc) {
        switch(sortBy) {
            case SORT_ORDER.dateAsc:
                return image1.creationDate < image2.creationDate;
            case SORT_ORDER.dateDesc:
                return image1.creationDate > image2.creationDate;
            case SORT_ORDER.modDateAsc:
                return image1.modifiedMS < image2.modifiedMS;
            case SORT_ORDER.modDateDesc:
                return image1.modifiedMS > image2.modifiedMS;
            case SORT_ORDER.fileSizeAsc:
                return image1.fileSize < image2.fileSize;
            case SORT_ORDER.fileSizeDesc:
                return image1.fileSize > image2.fileSize;
            case SORT_ORDER.fileNameAsc:
                return image1.shortName < image2.shortName;
            case SORT_ORDER.fileNameDesc:
                return image1.shortName > image2.shortName;
        }
    }

    //private
    shouldIncludeFile(fileName) {
        return FILE_EXT_PATTERN.test(fileName);
    }

    //private
    shortNameFromFullPath(fileName) {
        let split = fileName.split('/');
        return split[split.length - 1];
    }

    //private
    buildFileList(here) {
        let that = this;
        let allFiles = [];
        fs.readdirSync(here).filter(file => {
            return !file.startsWith('.') && !file.includes(' ');
        }).forEach(file => {
            let fileName = here.endsWith('/') ? `${here}${file}` : `${here}/${file}`;
            let stats = { isDirectory: () => {return false;}};
            try {
                stats = fs.statSync(fileName);
            } catch(ignore) {}
            if(stats.isDirectory()) {
                try {
                    allFiles = allFiles.concat(that.buildFileList(fileName));
                } catch(ignore) {}
            } else {
                allFiles.push(fileName);
            }
        });
        return allFiles.filter(this.shouldIncludeFile).sort(this.sortImages.bind(this));
    }

    //private
    generateStats(fileName) {
        let stats = {};
        try {
            stats = fs.statSync(fileName);
        } catch(ignore) {}
        return IMAGE_OBJECT_TEMPLATE(fileName, this.shortNameFromFullPath(fileName), stats.birthtime, stats.birthtimeMs, stats.mtime, stats.mtimeMs, stats.size);
    }

    //private
    generateStatsForList(fileList) {
        let result = [];
        fileList.forEach(fileName => {
            result.push(this.generateStats(fileName));
        });
        return result;
    }

    //private
    loadImageFiles() {
        let allImages = this.buildFileList('/Users/A1147203/Downloads');
        return this.generateStatsForList(allImages);
    }

    //public
    loadImages() {
        const SEVEN_DAYS = 7 * 24 * 60 * 60 * 1000;
        let metadata = db.getMetadata(NAMESPACE);
        if(new Date().getTime() - new Date(metadata.lastUpdate).getTime() >= SEVEN_DAYS || db.getAll(NAMESPACE).length == 0) {
            let allImages = this.loadImageFiles();
            let changedImages = [];
            allImages.forEach(image => {
                let dbImage = db.get(NAMESPACE, image.fullPath);
                if(!dbImage || dbImage.modifiedMS < image.modifiedMS) {
                    changedImages.push(image);
                }
            });
            db.putAll(NAMESPACE, 'fullPath', changedImages);
        }
        return db.getAll(NAMESPACE);
    }
}

export default new ImageFileService();