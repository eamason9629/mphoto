import fs from 'fs';

/**
 *  This is the internal cache for all namespaces.
 */
let dbCache = {};

/**
 *  This is the currently hardcoded location where namespace files will be written
 *  to in the filesystem. Will eventually be configurable.
 */
const DB_LOCATION = '/Users/A1147203/code/mphoto/db';

/**
 *  This is the template that the connector uses for namespaces.
 *  Adding or removing from this template is considered a breaking change and should
 *  require a version incrementation and migrating namespace data.
 */
const DB_TEMPLATE = () => {
    return {
        data: {},
        _metadata: {
            namespace: '',
            creationDate: new Date(),
            lastUpdate: new Date(),
            schemaVersion: 'V1'
        }
    };
};

class DBConnectorService {
    /**
     *  This constructor initializes the DB folder, if it doesn't exist already.
     */
    constructor() {
        if(!fs.existsSync(DB_LOCATION)) {
            fs.mkdirSync(DB_LOCATION, {recursive: true});
        }
    }

    /**
     *  PRIVATE
     *  This is a private method that calculates the location of the DB file for
     *  the given namespace.
     */
    constructDBLocation(namespace) {
        return DB_LOCATION + '/' + namespace + '.db';
    }

    /**
     *  PRIVATE
     *  This private method initializes the namespace in the cache and filesystem.
     */
    createNamespaceDB(namespace) {
        let newNamespace = Object.assign({}, DB_TEMPLATE());
        newNamespace.namespace = namespace;
        dbCache[namespace] = newNamespace;
        this.writeNamespace(namespace);
    }

    /**
     *  PRIVATE
     *  This private method will load a namespace DB into the cache, and create it if it
     *  didn't exist already.
     */
    loadNamespace(namespace) {
        let dbLocation = this.constructDBLocation(namespace);
        if(!fs.existsSync(dbLocation)) {
            this.createNamespaceDB(namespace);
        }
        let dbContents = JSON.parse(fs.readFileSync(dbLocation));
        dbCache[namespace] = dbContents;
    }

    /**
     *  PRIVATE
     *  This private method asynchronously writes the namespace DB to the file system.
     */
    writeNamespace(namespace) {
        let dbLocation = this.constructDBLocation(namespace);
        fs.writeFile(dbLocation, JSON.stringify(dbCache[namespace]));
    }

    /**
     *  PUBLIC
     *  This method returns the metadata for the given namespace.
     */
     getMetadata(namespace) {
        if(!dbCache[namespace]) {
            this.loadNamespace(namespace);
        }
        return dbCache[namespace]._metadata;
     }

    /**
     *  PUBLIC
     *  This method returns all objects in the namespace that pass the given
     *  query method (the method should return true/false.
     *  It will create the namespace if it didn't exist already.
     */
    queryNamespace(namespace, queryMethod) {
        return this.getAll(namespace).filter(queryMethod);
    }

    /**
     *  PUBLIC
     *  This method returns the entire dataset for a namespace in object form.
     *  It will create the namespace if it didn't exist already.
     **/
     getData(namespace) {
        if(!dbCache[namespace]) {
            this.loadNamespace(namespace);
        }
        return dbCache[namespace].data;
     }

    /**
     *  PUBLIC
     *  This method returns an array of all objects in the given namespace.
     *  It will create the namespace if it didn't exist already.
     */
    getAll(namespace) {
        if(!dbCache[namespace]) {
            this.loadNamespace(namespace);
        }
        return Object.values(dbCache[namespace].data);
    }

    /**
     *  PUBLIC
     *  This method returns the object with the given id in the given namespace.
     *  It will create the namespace if it didn't exist already.
     */
    get(namespace, id) {
        if(!dbCache[namespace]) {
            this.loadNamespace(namespace);
        }
        return dbCache[namespace].data[id];
    }

    /**
     *  PUBLIC
     *  This method inserts an object with the given id into the given namespace.
     *  It will create the namespace, if it didn't exist already.
     */
    put(namespace, id, data, doWrite = true) {
        if(!dbCache[namespace]) {
            this.loadNamespace(namespace);
        }
        dbCache[namespace].data[id] = data;
        dbCache[namespace]._metadata.lastUpdate = new Date();
        if(doWrite) {
            this.writeNamespace(namespace);
        }
    }

    /**
     *  PUBLIC
     *  This method inserts a list of objects into the given namespace. It uses the value of the idField as the id.
     *  It will create the namespace, if it didn't exist already.
     */
    putAll(namespace, idField, dataList) {
        if(!dbCache[namespace]) {
            this.loadNamespace(namespace);
        }
        dataList.forEach(data => {
            this.put(namespace, data[idField], data);
        });
        this.writeNamespace(namespace);
    }
}

export default new DBConnectorService();