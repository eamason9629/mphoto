import fs from 'fs';
import db from './DBConnectorService';

const NAMESPACE = 'navigation';
const INDEX_KEY = 'index';

let currentIndex = 0;

class NavigationHistoryService {
    constructor() {
        currentIndex = db.get(NAMESPACE, INDEX_KEY) || currentIndex;
    }

    screenChange(newScreen, priorState) {
        //the frist two falses make these writes transactional
        db.put(NAMESPACE, currentIndex, priorState, false);
        currentIndex++
        db.put(NAMESPACE, currentIndex, newScreen, false);
        db.put(NAMESPACE, INDEX_KEY, currentIndex);
    }

    currentScreen() {
        return db.get(NAMESPACE, currentIndex);
    }

    previousScreen() {
        return db.get(NAMESPACE, currentIndex - 1);
    }

    allHistory() {
        let data = db.getData(NAMESPACE);
        let keys = Object.keys(data).sort((first, second) => {
            return first > second;
        });
        let result = [];
        keys.forEach(key => {
            result.push(data[key]);
        });
        return result;
    }
}

export default new NavigationHistoryService();