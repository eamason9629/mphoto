* include EXIF file stats from https://github.com/exif-js/exif-js
* more accurate sorting of images

* search images
* configure application
* the whole concept of memories
* tagging images
* image notes/description



REFACTORS:
* a lot of repetition in DBConnector
* transactionality of DBConnector is nonexistant

BUGS:
* first time a db is open, a file not found error happens
